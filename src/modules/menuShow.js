const menuToggle = document.querySelector('.menu--toggle');
const menuClose = document.querySelectorAll('.menu__close');
const menuContainer = document.querySelector('.menu--mobile');
const overlayElement = document.querySelector('.overlay');
const mobileMenuShow = (event) => {
    const target = event.target;
    document.querySelector('.overlay').classList.add('overlay--show');
    menuContainer.classList.add('menu--mobile_show');
    menuContainer.classList.remove('menu--mobile--hidden');
};
const mobileMenuHidden = (event) => {
    const target = event.target;
    if(menuContainer.classList.contains('menu--mobile_show')) {
        menuContainer.classList.add('menu--mobile_hidden');
        overlayElement.classList.remove('overlay--show');
        setTimeout(() => {
            menuContainer.classList.remove('menu--mobile_show');
            menuContainer.classList.remove('menu--mobile_hidden');
            menuContainer.classList.add('menu--mobile--hidden');
        }, 500);
        return false;
    }
};

menuToggle.addEventListener('click', mobileMenuShow);

menuClose.forEach((button) => {
    button.addEventListener('click', mobileMenuHidden);
});

overlayElement.addEventListener('click', mobileMenuHidden);