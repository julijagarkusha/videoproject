const searchInput = document.getElementById('siteSearch');
const searchClick = document.querySelector('.form--click');

const searchFocus = (event) => {
    const target = event.target;
    target.parentNode.parentNode.classList.add('search--big');
    searchInput.focus();
    searchClick.classList.add('form--click_hidden');
};

const searchBlur = (event) => {
    const target = event.target;
    target.parentNode.parentNode.classList.remove('search--big');
    searchClick.classList.remove('form--click_hidden');
};

searchClick.addEventListener('click', searchFocus);
searchInput.addEventListener('blur', searchBlur);