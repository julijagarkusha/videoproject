import {inputFocus, inputBlur} from './inputFocus'

//popup show
const overlayElement = document.querySelector('.overlay');
const formShowButton = document.querySelectorAll(".auth .button");
const popupRegister = document.querySelector('.popup');
const popupForms = document.querySelectorAll('.popup__form');
const popupImg = document.querySelectorAll('.popup__img');
const popupTitle = document.querySelector('.popup__content h2');
const tabElements = document.querySelectorAll('.popup__switch');
const headerElement = document.getElementsByTagName('header')[0];
const menuMobileShow = document.querySelector('.menu--mobile');

const formShow = (event) => {
    const target = event.target;
    const popup = target.getAttribute('data-name');

    if(popup === 'logIn') {
        popupTitle.innerHTML = popupTitle.getAttribute('data-title-login');
    } else {
        popupTitle.innerHTML = popupTitle.getAttribute('data-title-signup');
    }

    popupForms.forEach(popupForm => {
        popupForm.classList.remove('popup__form--show');
        if(popupForm.getAttribute('data-name') === popup) {
            popupForm.classList.add('popup__form--show')
        }
    });

    popupImg.forEach(imgItem => {
        imgItem.classList.remove('popup__img--show');
        if(imgItem.getAttribute('data-name') === popup) {
            imgItem.classList.add('popup__img--show')
        }
    });

    const formInputs = document.querySelectorAll('.input__form');
    formInputs.forEach(input => {
        input.addEventListener('focus', inputFocus);
        input.addEventListener('blur', inputBlur);
    });
};

const popupShow = (event) => {
    event.stopPropagation();
    const target = event.target;
    formShow(event);
    const popup = target.getAttribute('data-name');

    tabElements.forEach(tab => {
        tab.classList.remove('popup__switch--active');
       if(tab.getAttribute('data-name') === popup) {
           tab.classList.add('popup__switch--active');
       }
    });
    headerElement.classList.add('popup--visible');
    popupRegister.classList.add('popup--show');
    overlayElement.classList.add('overlay--show');
    if(menuMobileShow.classList.contains('menu--mobile_show')) {
        menuMobileShow.classList.toggle('menu--mobile_show');
    }
};

const popupHidden = (event) => {
    const target = event.target;
    headerElement.classList.remove('popup--visible');
    popupRegister.classList.remove('popup--show');
    overlayElement.classList.remove('overlay--show');
};

const tabsToggle = (event) => {
    const target = event.target;
    formShow(event);
    tabElements.forEach(tabElement => {
        tabElement.classList.remove('popup__switch--active')
    });
    target.classList.add('popup__switch--active');
};

formShowButton.forEach((button) => {
    button.addEventListener('click', popupShow)
});

overlayElement.addEventListener('click', popupHidden);

tabElements.forEach(tabElement => {
    tabElement.addEventListener('click', tabsToggle)
});



