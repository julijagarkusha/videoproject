import {default as rangeSlider} from "rangeslider-pure";

const incomeInput = document.querySelector('.income__range');
const incomeValueElement = document.getElementById("incomeViews");

if(incomeInput) {
    rangeSlider.create(incomeInput, {
        min: 100000,
        max: 1000000,
        value: 320000,
        onSlide: (value) => {
            let valueFormat = new Intl.NumberFormat("ru");
            value = valueFormat.format(value);
            incomeValueElement.innerHTML = value;
        }
    })
}