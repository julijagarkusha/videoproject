import Glide from "@glidejs/glide";

const sliderElement = document.querySelector('.bloggers');
const sliderButtons = document.querySelector('.complain__buttons');

if(sliderElement) {
    new Glide('.bloggers', {
        type: 'carousel',
        perView: 4,
        focusAt: 2,
        gap: 80,
        peek: {
            before: 365,
            after: -225,
        },
        breakpoints:{
            2400: {
                perView: 3.5,
                focusAt: 2,
                gap: 80,
                peek: {
                    before: 365,
                    after: -225,
                },
            },
            2030: {
                perView: 3,
                focusAt: 2,
                gap: 80,
                peek: {
                    before: 365,
                    after: -225,
                },
            },
            1770: {
                perView: 2.8,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: 400,
                    after: -400,
                },
            },
            1600: {
                perView: 3,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: 300,
                    after: -400,
                },
            },
            1520: {
                perView: 2.4,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: 500,
                    after: -400,
                },
            },
            1440: {
                perView: 2.3,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: 470,
                    after: -400,
                },
            },
            1330: {
                perView: 2.2,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: 400,
                    after: -400,
                },
            },
            1220: {
                perView: 2.1,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: 350,
                    after: -400,
                },
            },
            1100: {
                perView: 2,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: 300,
                    after: -400,
                },
            },
            988: {
                perView: 2,
                focusAt: 1,
                gap: 20,
                peek: {
                    before: 250,
                    after: -400,
                },
            },
            930: {
                perView: 1.8,
                focusAt: 1,
                gap: 20,
                peek: {
                    before: 280,
                    after: -400,
                },
            },
            850: {
                perView: 1.5,
                focusAt: 1,
                gap: 20,
                peek: {
                    before: 200,
                    after: -200,
                },
            },
            800: {
                perView: 1.5,
                focusAt: 1,
                gap: 20,
                peek: {
                    before: 150,
                    after: -200,
                },
            },
            768: {
                perView: 1.85,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: -20,
                    after: -10,
                },
            },
            690: {
                perView: 1.7,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: -50,
                    after: 50,
                },
            },
            640: {
                perView: 2,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: -200,
                    after: 50,
                },
            },
            585: {
                perView: 2,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: -260,
                    after: 35,
                },
            },
            530: {
                perView: 2,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: -280,
                    after: 35,
                },
            },
            500: {
                perView: 2,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: -300,
                    after: 35,
                },
            },
            480: {
                perView: 2.3,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: -150,
                    after: 35,
                },
            },
            440: {
                perView: 2.1,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: -170,
                    after: 35,
                },
            },
            380: {
                perView: 2,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: -200,
                    after: 35,
                },
            },
            320: {
                perView: 2,
                focusAt: 2,
                gap: 20,
                peek: {
                    before: -210,
                    after: 35,
                },
            },
        }
    }).mount();
}


class MobileCarousels  {
    constructor(selector, config = {
        type: 'carousel',
        perView: 1.3,
        focusAt: 0,
        gap: 20,
        peek: {
            before: 20,
            after: -20,
        },
    }) {
        this.selectorQuery = selector;
        this.config = config;
    }

    status = false;
    sliderInstances = [];

    destroy() {
        this.status = false;
        this.sliderInstances.forEach(slider => {
            slider.destroy();
        });
        this.sliderInstances = [];
    }

    init() {
        this.status = true;
        document.querySelectorAll(this.selectorQuery).forEach((node) => {
            const slider = new Glide(node, this.config);
            slider.mount();
            this.sliderInstances.push(slider);
        })

    }
}

const carousels = new MobileCarousels('.appeals');

const tabs = new MobileCarousels('.tab__buttons', {
    type: 'carousel',
    perView: 1.3,
    focusAt: 0,
    gap: 20,
    peek: {
        before: 0,
        after: 0,
    },
});
const mobilePointWidth = 767;

window.addEventListener('resize', (e) => {
    if (window.innerWidth < mobilePointWidth && !carousels.status) {
        carousels.init();
        tabs.init();
    }

    if (window.innerWidth > mobilePointWidth && carousels.status) {
        carousels.destroy();
        tabs.destroy();
    }
});

if (window.innerWidth < mobilePointWidth) {
    carousels.init();
    tabs.init();
}