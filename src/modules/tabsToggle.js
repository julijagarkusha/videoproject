const tabElements = document.querySelectorAll('.tabs__item');
const tabVideo = document.getElementById('tabsVideo');
const tabPlayList = document.getElementById('tabsPlaylist');
const tabInfo = document.getElementById('tabsInfo');
const tabContentElements = document.querySelectorAll('.tabs__content');

const tabsToggle = (event) => {
    const target = event.target;
    tabElements.forEach(tabElement => {
        tabElement.classList.remove('tabs__item--active');
        target.classList.add('tabs__item--active');
    });
    tabContentElements.forEach(tabContentElement => {
        tabContentElement.classList.remove('tabs__content--show');
        if(target.getAttribute('data-name') === 'video--show') {
            tabVideo.classList.add('tabs__content--show')
        } else if (target.getAttribute('data-name') === 'playlist--show') {
            tabPlayList.classList.add('tabs__content--show')
        } else {
            tabInfo.classList.add('tabs__content--show')
        }
    });
};

if(tabElements) {
    tabElements.forEach(tabElement => {
        tabElement.addEventListener('click', tabsToggle);
    });
}
