export const inputFocus = (event) => {
    const target = event.target;
    target.classList.add('input--focus');
    console.log('target')
};

export const inputBlur = (event) => {
    const target = event.target;
    target.classList.remove('input--focus');
};
