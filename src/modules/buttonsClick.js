const clickButtons = document.querySelectorAll('.complain__button');

const buttonClick = (event) => {
  const target = event.target;
    clickButtons.forEach(clickButton => {
        clickButton.classList.remove('button--solid');
    });
    target.classList.add('button--solid');
};

clickButtons.forEach(clickButton => {
   clickButton.addEventListener('click', buttonClick);
});
