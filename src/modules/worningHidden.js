const warningBanner = document.querySelector('.warning');
const footer = document.querySelector('footer');
if (!localStorage.getItem('cookiesWarning')) {
    warningBanner.classList.add('warning--show');
    footer.classList.add('warning_open');
}
document.querySelector('.agree_cookie_warning').addEventListener('click', () => {
    warningBanner.classList.remove('warning--show');
    footer.classList.remove('warning_open');
    localStorage.setItem('cookiesWarning', '1');
});