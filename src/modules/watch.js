const videoElement = document.getElementById('videoWatch');
const videoPlayElement = document.getElementById('videoPlay');

if(videoElement) {
    videoPlayElement.addEventListener('click', () => {
        console.log('video');
        if (videoElement.paused) {  // если видео остановлено, запускаем
            videoElement.play();
        } else {
            videoElement.pause();
        }
    }, false);
}
