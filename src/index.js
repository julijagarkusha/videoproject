import Glide from '@glidejs/glide';
import Choices from 'choices.js';
import { default as rangeSlider } from 'rangeslider-pure';
import "choices.js/public/assets/styles/choices.min.css";
import videojs from 'video.js';
import "./styles/index.scss";
//import '!style-loader!css-loader!video.js/dist/video-js.css';
//import '@videojs/themes/dist/city/index.css';
import "./modules/popupShow";
import './modules/popupTabsToggle';
import './modules/menuShow';
import './modules/watch';
import './modules/searchFocus';
import './modules/inputFocus';
import './modules/buttonsClick';
import './modules/tabsToggle';
import './modules/rangeDraw';
import './modules/sliderInit';
import {isArray} from '@glidejs/glide/src/utils/unit';
import './modules/videoPlayerCreate';

const videoElement = document.getElementById('video1');

if(videoElement) {
  const player = videojs('video1', {
      controlBar: {
          progressControl: {
              keepTooltipsInside: true
          }
      },
      controls: true, // включить кнопки на плеере
      autoplay: true, // Автозапуск
      preload: 'auto', // Загрузка видео
      loop: true, // Повтор видео
      fluid: true,
      techOrder: ["html5"],
      nativeControlsForTouch: true, // Использовать родные мобильные кнопки
  }, function() {
      console.log();
  });
    videojs.log.level('all');
}

const selectCustom = document.getElementById('complainOptions');
const selectProblems = document.getElementById('problemOptions');
if(selectCustom) {
    new Choices(selectCustom, {
        searchEnabled: false,
        shouldSort: false
    });
}

if(selectProblems) {
    new Choices(selectProblems, {
        searchEnabled: false,
        shouldSort: false
    });
}

const documentWidth = window.innerWidth;
console.log(documentWidth);
const langSelector = document.querySelector('.select__lang');

//outside event listener
document.addEventListener('click', (event) => {
    langSelector.classList.remove('select__lang--open');
    searchResult.classList.remove('search__result--show');
    setTimeout(() => {
        langSelector.classList.add('select__lang--close');
    }, 500);
});

//lang select click listener
if(langSelector) {
    langSelector.addEventListener('click', (event) => {
        event.stopPropagation();
        langSelector.classList.toggle('select__lang--open');
        langSelector.classList.remove('select__lang--close');
    });
}

const searchInput = document.getElementById('siteSearch');
const searchResult = document.querySelector(".search__result");

searchInput.addEventListener('input', () => {
    if(searchInput.value.length >= 3 && !searchResult.classList.contains("search__result--show")) {
        searchResult.classList.add('search__result--show');
    } else if (searchInput.value.length < 3 && searchResult.classList.contains("search__result--show")) {
        searchResult.classList.remove('search__result--show');
    }
});

//search results click
const searchResults = document.querySelectorAll('.search__item');
const resultOption = (event) => {
    const target = event.target;
    event.stopPropagation();
    searchInput.value = target.innerHTML;
    searchResult.classList.remove('search__result--show');
};

searchResults.forEach((item) => {
    item.addEventListener('click', resultOption);
});

